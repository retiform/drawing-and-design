# README
----

### Why I Chose a Minimal Design

Not only do minimal designs look amazing, they deal with a lot of issues in web design. Originally, I wanted to go with a Material Design theme. However, that meant dealing with a lot of extra images, slowing down load times, and a material design theme would also employ a lot of JS and CSS that might not work on edge case browsers, or, in the case of JS, simply be blocked for privacy reasons. Finding a bunch of images and icons that matched was also a huge pain and a waste of my time, given that this is some random portfolio.


### Setting up without Gitlab Build

* install hugo 
* Remove public/ from .gitignore
* cd to projectDir
* run hugo

hugo server can be used to test it on local host, but it will automatically build 
your website instead of using the built one in public/ 

### Misc notes
note: the many commits is from testing settings for breaking changes in "production" (the site is/wasnt up then)
