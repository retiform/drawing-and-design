+++
date = "2018-09-01"
title = "Post 1"
slug = "adv-post-1"
tags = [
    "CAD",
	"case",
	"phone",
	"zero phone",
	
]
categories = [
    "CAD",
]
+++

Currently, I am working on creating a phone body for the Zero Phone, an experimental development phone based on the
raspberry pi zero board.  I am interested in creating a case for the project, because I both support its goals (to
make a more open phone), and because it is well documented yet lacks any 3D printable cases. The case will feature
an unique body designed to cling to the sides of the zero phone, because the battery pack does not really have any
mount points.  The phone housing will also have to be easily removed from the body of the phone, to allow for
repairs, part replacement, battery replacement, etc.  Offering a battery pack plate may be advisable, because the
pi phone uses 18650 cells for power, and they will not come with the phone to avoid shipping costs.  The cells also
make the parts easier to purchase, because they have been standardized, unlike cell phone batteries. Image of the
current model of pi phone can be found bellow.

![Zero Phone](images/zerophone-side.jpg)
