+++
date = "2018-09-25"
title = "Post 3.5: Transfer To Hugo Complete"
slug = "hugo-transfer"
tags = [
    "go",
    "golang",
    "templates",
    "themes",
    "development",
	"hugo",
	"website"
]
categories = [
    "Development",
    "golang",
]
+++

After many hours of work, I finally managed to deprecate my Weebly page and replace it with Hugo and Gitlab
Pages. Weebly worked, but it was a painful solution as the Weebly editor is slow, unreliable, and buggy. It also
took way to long to change, and there was no way to integrate with [Git](git-scm.com) my version control software
of choice.  Requiring internet was not helpful either. (Actually, I do not believe there was any form of version
control what so ever). The theme selection was a bit limited as well, and themes were a pain to modify.

### How Hugo Solves my Problems.

Hugo, a static site generator, takes a set of post, theme, configuration and image files, then turns them into a
static website made entirely of CSS+HTML and JS (if your theme uses JS). One you are done setting it up, a quick
run of the Hugo command spits out a website for you. Then I can just run git push on the ./public folder. However,
Gitlab also supports building your website for you, so I will try that out and see how it goes for me. For the
graphic design, I used the [Coder Portfolio](https://themes.gohugo.io/hugo-coder-portfolio/) theme by Yusuke
Ishimi. Hugo's biggest draw for me though, is that it supports [Markdown](https://en.wikipedia.org/wiki/Markdown)
syntax for writing posts in. Markdown is a lot easier to use than HTML+CSS, but is still store in plain text
files. Thus, I can simply open the Emacs text editor and just get to work, without having to worry about dealing
with formatting issues, publishing, etc. Plain text files also give me the ability write my posts without even
using a GUI on my computer, if I wanted/needed to. Plain text files are also easy to write, and easy backup, save,
and transfer to whatever web builder program I decide to use next.


### Why I Chose a Minimal Design

Not only do minimal designs look amazing, they deal with a lot of issues in web design. Originally, I wanted to go
with a Material Design theme. However, that meant dealing with a lot of extra images, slowing down load times, and
a material design theme would also employ a lot of JS and CSS that might not work on edge case browsers, or, in the
case of JS, simply be blocked for privacy reasons. Finding a bunch of images and icons that matched was also a
huge pain and a waste of my time, given that this is some random portfolio.

![picture](images/tetrahedronFrame.png)
