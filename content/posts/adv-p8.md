+++
date = "2018-10-25"
title = "Post 8"
tags = [
    "zero phone",
    "CAD",
	"case",
	"phone",
]
categories = [
    "CAD",
]
+++

I continued to work on the case the design, and built some emergency code for the robotics team. For the case, I
added a port hole for the audio jack (not shown in the picture bellow), and begain to look into how the speaker is
placed on the phone, in order to make sure my case does not interfer with GSM calls. Some other minor fixes to the
case were made so that it would actually fit around the phone. I also needed to get the fisrt code to run on the
robotics team robot compiled quickly so the robotics team would know if the robot was working (it is), so that took
up one of my days. One of the machines I intended to build on was having unexplained networking issues, so I had to
jump over to another. I resolved a few bugs, and setup some hardware names on the robot's hardware controller so my
code would work with it.


Picture of the case from earlier in the week.

![Beginning of Case](images/hardcase.png)
