+++
date = "2018-09-17"
title = "Post 3"
slug = "adv-post-3"
tags = [
    "ftc",
    "robotics",
	"first",
	
]
categories = [
    "first",
    "robotics",
]
+++

This week, I continued to read up on the FTC java docs.  They are fairly simple, and fortunately similar to the FRC
API.  A reference guide was found [here.](https://github.com/ftctechnh/ftc_app) Only major problem I have is that
there are a massive amount of packages, classes, etc to look through.  However, now that robotics is in session, I
will save that for team meeting. I can now resume work on my phone body. I still lack the required meassurement,
but am hoping I can resolve my issue by the end of next week.

![Git Tree](images/git-branch.png)
