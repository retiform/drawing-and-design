+++
date = "2019-03-02"
title = "Post 27 More rolling dice"
tags = [ 
	"Dice",
	"CAD",
	"Product"
]
categories = [
   "phone case", "zerophone"
]
+++

I rolled more dice today. My new design has been broken into two catagories: trying to make dice bounce less, and
trying to make dice experince more friction as it slies accross the surface it is bouncing on. I predict that hte
latter version will likely be a more effective tool at slowing down the dice, but either could work. I printed
another dice for the purpose of this testing to avoid dealing with losing or damaging my orignal dice. 

![By Paolo Cignoni - Own work, CC BY-SA 4.0, https://commons.wikimedia.org/w/index.php?curid=57953642](images/900px-Schematic_representation_of_Fused_Filament_Fabrication_01.png)
