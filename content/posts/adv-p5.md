+++
date = "2018-10-04"
title = "Post 5"
tags = [
    "zero phone",
    "CAD",
	"case",
	"phone",
]
categories = [
    "CAD",
]
+++

I spent this week modeling the battery pack and thinking of ways to keep the phone from bouncing around insde the
housing. Currently, I am investigating using a silicone layer inside of the case to keep the phone from bouncing
around too. much. Silicone is cheap, light, thin, and relatively resistant to heat (assuming you purchase a
temperature resistant varient). As an added bonus, it should help protect the phone some during impact. However,
the silicone is not without downsides. The casing would need silicone sheets placed within a plastic shell in order
to hold the silicone in place, adding to the complexity of constructing it. (Though the Zero Phone requires several
hours to source assemble and get working, so I can not imagine that being a huge showstopper.) An alternative I
will also look into is simply using a flexible 3d printed plastic. The battery pack also managed to eat a good
chunk of my time, as tracking down the all dimensions and drawing it took a bit over 40 minutes.

![The Reference Model With Battery](images/zerophoneRefenceBodyBatModel.jpg)

