+++
date = "2019-02-15"
title = "Post 25 Printer Fight"
tags = [ 
	"Dice",
	"CAD",
	"Product"
]
categories = [
   "phone case", "zerophone"
]
+++

I have done next to nothing, but fight with a 3D printer. I created a 3D model of a 16mm 1d4 (normal dice), and now
want to print it. Unfortunately, the printer: smashed by first print attempt, slid around with my second attempt,
and then broke on my third attempt. After that, I tried unclogging it, which failed. Durring the unclogging, the
printer software broke somehow, and then I needed to run a firmware update. I also reajusted the heater to make
sure it was alighned properly. 


![By Paolo Cignoni - Own work, CC BY-SA 4.0, https://commons.wikimedia.org/w/index.php?curid=57953642](images/900px-Schematic_representation_of_Fused_Filament_Fabrication_01.png)
