+++
date = "2019-01-11"
title = "Post 18"
tags = [ 
	"FRC",
	"java",
	"gradle"
]
categories = [
   "java"
]
+++

This week I worked on finishing up my case. I have come to the conclusion that screws are now the best way to affix
the lid to my phone case. Hopefully, the case, which has been delayed months from its creation by the half-dozen
programs I wrote in this class for the FTC and FRC robots, will finally be compleate and I can get feedback from
the zerophone creators and users. The Zerophone community, according to a recent blog post, is very interested in a
phone case for the zero phone, but the creator of the zerophone has been unable to produce one as of yet, making
my efforts not in vain. I am a bit worrried that the case is going break from the screws inside of it, but I have
been making the plastic way too thick, and the case wont offer that much protection anyways.  

![case lid features](images/casetop.png)
