+++
date = "2019-02-13"
title = "Actualy a post on physics P23"
tags = [ 
	"ZeroPhone",
	"CAD",
	"Product"
]
categories = [
   "phone case", "zerophone"
]
+++

In my quest to design a normal dice, I have had to spend my week reading physics papers. Of particular note,
the paper _Can A Dice Be Fair by Dynamics_ has detailed an explanation of the motor of a ballanced dice. Since
that is exactly the opposite of what I want, I am attempting to use it to determine how to weight a dice in a
predictable manner. The article _Weldon's Dice Automated_ has provided me with a useful tidbit of information:
the distrubtion of rolls for each face of plastic dice. (It concluded that pips were not a sigificant factor
in affecting the distrubiton of dice rolls). As a result, I can safely say that simply removing small amounts
of weight will not work.


![Nanami Kamimura derivative work: SiPlus - Die_Faces.png ](images/Dice_Faces_svg.png)
