+++
date = "2019-03-04"
title = "Post 29 More Studing for Solid Works"
tags = [ 
	"Dice",
	"CAD",
	"Product"
]
categories = [
   "phone case", "zerophone"
]
+++

There really is not much for me to say, since I am just studing for Solid works. I did some of the practice exam
problems, and also studied the more of the Solid works commands. I am really missing the view cube from Inventor. Solid
works has one, but I have not gotten used to the keybinds to activate it yet. The editing features in Solidworks is much
more effective in Solidworks, presuemably due to the way it handles relationships.


![A shape](images/tetrahedronFrame.png)
