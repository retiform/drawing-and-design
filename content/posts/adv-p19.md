+++
date = "2019-02-05"
title = "Project proposal"
tags = [ 
	"FRC",
	"java",
	"gradle"
]
categories = [
   "java"
]
+++

My proposal for my first project of 2019, is to create a normally distrubuted dice, to allow RPG players to use
normal distrubutions without needing a laptop or phone. To do this, I will need to create a weighted die, that
useually rolls towards its mean value. The die will probably need to have about 30 sides or above, to make it
approximately use a z score as its critical value. I will then need to test the part, and determine if it is
actually normal. I will also need to work out the geometery or weighting system that will allow a dice to be
consistantly biased towards certain values, and values near those values.

![public domain normal distrubution curves from wikimedia commons](images/1024px-Normal_Distribution_PDF.svg.png)
