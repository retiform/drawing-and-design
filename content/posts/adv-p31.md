+++
date = "2019-03-07"
title = "Post 31 Even More Studing for Solid Works"
tags = [ 
	"Dice",
	"CAD",
	"Product"
]
categories = [
   "phone case", "zerophone"
]
+++

This time, I went over the types of views in drawings. I knew what they were, but it has been so long since I have done
a full drawing that I needed a refresher. Overall there really is not anything interesting to report here. I dont even
really have any images, since they would just be of a computer screen with a pdf open.

![A shape](images/tetrahedronFrame.png)
