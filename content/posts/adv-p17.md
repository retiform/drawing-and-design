+++
date = "2019-01-10"
title = "Post 17"
tags = [ 
	"FRC",
	"java",
	"gradle"
]
categories = [
   "java"
]
+++

This week I managed to get some more work in the Zerophone case, by making it more ergonomic and able to finally fit the phone. I also worked on testing some programming languages for the FRC team, but this will likely be the last week that I work on the robotics stuff in this class. The things I tested for the frc team are the new FRC plugin/build system, and robotpy. Robotpy works on the computers here, but the java build system unfortunatley does not. As for the zero phone case, I need to add I cover of some sort to hold it in.

![Gradle](images/Gradle_Logo.png)
