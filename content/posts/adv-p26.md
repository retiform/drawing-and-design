+++
date = "2019-02-17"
title = "Post 26 Rolling Dice!"
tags = [ 
	"Dice",
	"CAD",
	"Product"
]
categories = [
   "phone case", "zerophone"
]
+++

I printed the dice after fixing the printer. I then rolled the dice a total of 358 times. Not very many times, but
I was unable to find significant evidence that the dice were biased. For a chi-squared goodness of fit test
(assuming that a "correct dice" is one that rolls on each side evenly) I got the following values X-squared =
5.9218, df = 5, p-value = 0.3139. To save the trouble, the most important one there is the p-value, which
represents the probablity that a distrubution of rolls as extreme as the one I found, would occur randomly after
358 rolls over 6 categories (dice have 6 sides). Since the p-value is .3139, that means that values as extreme as
mine should be expected (from a fair dice), one out of three times. This can be interpreted as suggesting that my
dice are probably mostly fair. If there is bias in my dice, I would likely need to hundreds, if not thousands of
times to find a problem. (Regardless, most real dice are not fair and have _some_ imperfections, you simply would
not notice it) 


![By Paolo Cignoni - Own work, CC BY-SA 4.0, https://commons.wikimedia.org/w/index.php?curid=57953642](images/900px-Schematic_representation_of_Fused_Filament_Fabrication_01.png)
