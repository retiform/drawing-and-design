+++
date = "2018-09-17"
title = "Post 2"
slug = "adv-post-2"
tags = [
    "ftc",
    "robotics",
	"first",
	"git",
	"gitlab",	
]
categories = [
    "first",
    "robotics",
	"git",
]
+++

I have been sidetracked from the phone case work in order to work on the school's new FTC team.  Specifically, I
have been tasked with setting up the [gitlab](http://gitlab.com/) for the team, and figuring out how to program FTC robots in Java (which
are, of course, different from FRC robots).  The Gitlab part was created to deal with all of our code, using git.
The part I am most excited about is that we will finally be able to do proper code reviews now that we are using
actual code management tools.  (The FRC team used labview, but it was a mess and refused to work with any kind of
version control, even when we brought in a computer science grad student for advice.)  I also found the
documentation for the FRC API in Java, which took longer than it should have.  Gitlab logo below, because I have no
idea what to put as a picture.

![Git Lab](images/gitlab-logo.png)
