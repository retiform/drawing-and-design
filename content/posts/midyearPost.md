+++
date = "2019-01-20"
title = "Midyear post"
tags = [ 
	"FRC",
	"java",
	"gradle"
]
categories = [
   "java"
]
+++

### The Zero Phone Case

-------- 

I have done quite a bit over the year so far. At the start of the year, I started working on a case for an
experimental phone that, at the time, had no public dimension documents, and was a mess of soldered together PCBs
and wires. That phone was the Zerophone, which interested me, because of its goal of creating the first phone for
sale built around a Rasberry pi. It had no case created for it. At first I tried using pictures of the Zerophone,
and Trigonometry (along with the known dimensions of certain parts) to estimate the full dimensions of the
Zerophone. Never do that, it made me miserable, took weeks, and was wildly inaccurate, since I knew nothing about
compensating for distortions in the pictures. Eventually, I just gave up, and desided to hunt down Orthographic
drawings or CAD files of all the Zerophone pieces. I found a bunch of CAD files for the parts, but they were all
made for KiCAD, an excellent _electronics_ CAD program for PCB design, not mechanical design. As a result, I had to
download it, learn how to use it, and eventually, I figured out how to export all the files I wanted in a manner
that would make them easy to use in a mechanical design program. It took me a several weeks of going through
documentation, two emails to the creators, and then 3D modeling a battery case from a poorly made orthographic
drawing (blueprint), but I finally got the dimensions for the phone around month 2 of the year, after the project
lead emailed me a completed CAD file for the phone along with a bunch of pictures of measurements of the phone. The
battery case was missing from it, so I just modeled a rough model of the battery case + batteries using some
Orthographic drawings provided by the manufacturer. The creators released the files, that they gave me, publicly, a
few weeks later. Finally, was able to reference the dimensions to create a basic case design, and I then spent the
next few weeks refining it. I also researched how to properly protect the Zerophone, and settled on using a case
design that made the user place silicone sheets inside to protect the phone. (The zerophone, is a very brick like
phone, so casing it in any material was difficult).

### Robotics Season

----------- 

After that, robotics season got started, and since the team desperately needed working code, I was allowed to work
on the robotics code as a project in class, despite the fact that this delayed the finishing touches of my phone
case. I created several autonomous programs, a drive program, and then setup and tested build environment, for
another robot. The programming took up most of the end of 2018. To program the robot, I had to learn a number of
things about Gradle (ports used, setting it up, etc), how to properly fix git merge conflicts (when two lines of
code conflict each other while merging code together), and how to deploy code to an Android device (the brain of
our robot for FTC. combing code together). I also learned a lot about how java does object oriented programming and
how to properly use constructors in Java. (Which I had previously, rarely used). 


### More Zerophone

After I finished with most of the robotics code, I went back to the case in January, and worked on adding a way to
hold the phone better into the case. That was a lot harder then it sounds, because, from a mechanical standpoint,
the Zerophone is poorly designed, at least for anyone looking to build a housing for it. Eventually, I started
adding threaded holes for screws (pictured bellow). It is not quite done, and adds a concerning amount of work to
the case, but the phone is already only for a market that enjoys DIY construction. In the rest of the year, I hope
to get the case design tested, and see how I did. Additionally, I would like to work on building something bigger,
perhaps a sleeping tube, table, or maybe a wearable electronic device.



Pictured bellow: the case, the zerophone, and a git branch model, a tool used to help make the code.

![case lid features](images/casetop.png)

![zero phone](images/zerophone-side.jpg)

![a picture of git branches](image/git-branch.png)
