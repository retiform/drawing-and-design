+++
date = "2018-12-20"
title = "Post 16"
tags = [ 
	"FRC",
	"java",
	"gradle"
]
categories = [
   "java"
]
+++
I got into a fight with a code building system called Gradle, while testing the new code for FRC that was released in alpha.
Unfortunately, I kept getting dependency errors from things I needed but did not have installed, because FRC does not provide
amazing dependency documentation.

I also did some more rigourous testing of the the FTC automous. 

![Beginning of Case](images/Gradle_Logo.png)
