+++
date = "2018-11-03"
title = "Post 9"
tags = [
    "zero phone",
    "CAD",
	"case",
	"phone",
	"java"
]
categories = [
    "CAD","java"
]
+++

This week I finished up more of the robotics team code so that we would be ready by our test competition. I also
attempted to run some static analysis on the phone case in inventor, but I failed to get any useful data. In the
code, I pushed a number of commit relating to controlling motors, power directions, and fixing the drive
controls. I also compiled several versions of the code so it could be placed on the robot's brain. See below for a
picture of the java mascot because I need an image.

![Beginning of Case](images/guy.jpeg)
