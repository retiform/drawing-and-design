+++
date = "2019-03-03"
title = "Post 28 Studing for Solid Works"
tags = [ 
	"Dice",
	"CAD",
	"Product"
]
categories = [
   "phone case", "zerophone"
]
+++

I have decided to study for the SolidWorks Associates Exam. To do this, I am starting by learning the features of
Solidworks, because I have not used Solidworks that much in the past. This week, I created several parts that I done in
the past. The only difference was that I was working in Solid works. It seems to be a much more fluid editor than
Autodesk Inventor, but I am still not used to it.



![A shape](images/tetrahedronFrame.png)
