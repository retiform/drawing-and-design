+++
date = "2018-10-19"
title = "Post 7"
tags = [
    "zero phone",
    "CAD",
	"case",
	"phone",
]
categories = [
    "CAD",
]
+++

To get some kind of refernce model of my the phone I am trying to make a housing of, I created a model of the phone
that could be 3D printed, with most of the support structures and pin out sockets on the real phone shown on the 3D
model. I then 3D printed the model of the phone, and I still am not done cleaning the support structures off of
it. I also started designing the case. My first case prototype design will be a hybrid soft and hard body design,
with the hard parts being added by 3D printing the case, and the soft parts being made of silicone. I have not done
too much work on it yet; there are not even fillets on it. It should be done in the next two weeks. There is a 1 mm
space added so the silicone will fit between the hard part and the phone.

![3D Printable Model](images/zerophoneRefenceBodyBatModelFor3dPrint.png)


