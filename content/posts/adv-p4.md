+++
date = "2018-09-28"
title = "Post 4"
tags = [
    "zero phone",
    "CAD",
	"case",
	"phone",
]
categories = [
    "CAD",
]
+++

I finally managed to figure out the dimensions of the Zero Phone, because the Zero Phone creator responded to an
email I sent to ask about the zero phone dimensions. Not only did he tell me the general dimensions of the phone,
but he also took numerous reference photos with measurements in them, and provided me with a reference CAD model of
the phone, that someone was using to make case prototypes with, though the prototypes have not been released as the
creator felt the case had too many issues. The prototype case model was also included, but I probably wont be
referencing it much, because it might be designed for a different battery pack, and was brick shaped. Either way, I
now every thing I need to design my case. Bellow you can view the cad model of the zero phone, and one of the
photos that was sent to me.

![The Reference Model](images/zerophoneRefenceBodyandUSBOnlyCopy.png)

![The Dimension Photo](images/zerophoneSideDim.jpg)
