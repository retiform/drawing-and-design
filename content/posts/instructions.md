+++
date = "2019-02-11"
title = "Instructions For the Null Case, Part 1, a Forward [Post 21]"
tags = [ 
	"ZeroPhone",
	"CAD",
	"Product"
]
categories = [
   "phone case", "zerophone"
]
+++

### The Motivations for the Design, or Why it Seems Insane:

It is finnally done, my prototype housing for the Zerophone (a Pi zero based phone), something that should have
taken, maybe a week, but took serval months to finish. It is the Null-Case (a pun-name I gave it). The housing has
a number of issues: for one, it uses parts that someone is unlikely to have lying around, but are not avaible in
amounts that cost less $5-10 USD per part. The upside at least, is that once you have made one Null-Case the second
Null-Case is much cheaper to create. Hurray for replaceablity! The other major issue with the case is that it lacks
a full-face cover for the phone, because I have been more concerned with creating a case. Finishing touches can be
added later. The case also obscures most of the ports, to keep the phone case sturdy, so people who want acess to
all the phones ports will need to cut more holes into the 3D model before printing. Now for what the actually does
do.

The largest issue I have had, is that the zerophone, is roughly brick shaped making a flexable case nearly
imppossible to attach, but also lacks any mount points for an external case, like one would normally expect from a
device shaped like a TV remote. The case had to be somewhat easy to remove, but also to not break over time, and
tight enough to prevent the phone from rattling around. To make the phone easy to manufacture, as well as cheap and
somewhat disposable, I decided to make it 3D printed, as 3D printers are now common, relatively cheap to gain
access to, and the Zerophone's market (hackery DIY people) are probably more likely to have access to one than say,
a CNC machine, sheet metal cutting tools, or a laser cutter, thanks to libraries having cheapish 3D printers.

Cheap 3D prints are, as a general rule, made of either ABS or PET plastic. There are other (mostly worse) plasics,
but they are all fairly similar, in that none of them are particularly flexable, and therefore, would need to be
incredibly precise to get a tight fit with the zero phone, while still being easy-ish to remove. To solve this
problem, I have designed the case to allow the user to insert high temperature silicone between the case and the
phone. High temperature silicone is cheap, widely avaible, and comes in an array of sizes. The Null-Case is
designed with 2 cm thick high temperature silicone in mind. The silicone should be cut to fit the case and then
glued to the sides of the case. (see Instructions later on). The case opens from the top, due to the shape of the
Zerophone. A panel for easy access to the battery case could be added later on.

I will need to send in the files to the creators to get a full test. Next week, I will talk about how to actually
assemble the project.



![theCase](images/1024px-Normal_Distribution_PDF.svg.png)
