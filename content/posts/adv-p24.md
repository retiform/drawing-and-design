+++
date = "2019-02-13"
title = "Post 24, list of materials"
tags = [ 
	"ZeroPhone",
	"CAD",
	"Product"
]
categories = [
   "phone case", "zerophone"
]
+++

This week I have created a new setup for a FRC robot, that may or may not allow it to semi-automously drive itself into targets. Unfortantely,
our old robot is very broken. 

### And now, for a list of materal for the zero phone case.

To construct the zerophone case, one requires

* A small amount of high temperature silicone: just obtain 1/16 inch thick, or 2mm silicone. This cushions the boards in the phone case/housing.

* 4 m2.5 screws 8mm long.

* 8mm long brass threaded heat mold injection inserts. Should be no more than 3.5mm in diameter.

* PLA, ABS, or PET plastic to print the case with. A 3D printer will also be required. Othere plastics may work, but those are the most common.


* Some superglue or any glue that will bind to plastic and metal.


![The Phone](images/zerophoneSideDim.jpg)
