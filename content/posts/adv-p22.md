+++
date = "2019-02-11"
title = "Completion of the prototype, Java, and Physics [22]"
tags = [ 
	"ZeroPhone",
	"CAD",
	"Product"
]
categories = [
   "phone case", "zerophone"
]
+++

I will now deley the assembly instructions further, since I have finally managed to assemble this. As it turns out,
the case is lightly too long toward the bottom of the phone. Other than that, the case seems work fine, though I
have not, as of yet, obtained the threaded heat inserts that I will use to provide screw hooles for the case, as
they take around a month to ship, and I need to verify some things for order. I have sent in the zerophone case, to
the Zerophone project. They have provided no comment yet. Hopefully, this will be able to provide an effective case
for zerophone users.



![theCase](images/prototypeCase0.jpg)
