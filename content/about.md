+++
title = "About"
slug = "about"
thumbnail = "images/tn.png"
description = "about"
+++

# About Me

I am a student in Engineering Drawing, president of my school's computer club, an open source contributer and an all around tech nerd. I am adept in several CAD programs, including Inventor, AutoCAD, LibreCAD, and FreeCAD. I have the Autodesk Associate Certification in Inventor.
